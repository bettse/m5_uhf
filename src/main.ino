
#include <Arduino.h>
#include "wifi.h" //Wifi credentials
#include <HTTPClient.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <WiFiMulti.h>
#include <M5StickCPlus.h>
#include "RFID_command.h"

UHF_RFID RFID;

String comd = " ";
CardpropertiesInfo card;
ManyInfo cards;
SelectInfo Select;
CardInformationInfo Cardinformation;
QueryInfo Query;
ReadInfo Read;
TestInfo Test;

long previousMillis = 0;
long interval = 60 * 1000;

void setup() {
  M5.begin(true, true, true);
  RFID._debug = 1;
  Serial2.begin(115200, SERIAL_8N1, 33, 32);

  M5.Lcd.setRotation(1);
  M5.Lcd.setTextSize(2);

  // Connect WiFi
  M5.Lcd.println("Connecting to WiFi");
  Serial.println("Connecting to WiFi");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.print(".");
    Serial.print(".");
    delay(100);
  }
  M5.Lcd.println(" CONNECTED");
  Serial.println(" CONNECTED");

  String soft_version;
  soft_version = RFID.Query_software_version();
  while (soft_version.indexOf("V2.3.5") == -1) {
    Serial.print("Version not V2.3.5, got [");
    Serial.print(soft_version);
    Serial.println("]");
    RFID.clean_data();
    RFID.Delay(150);
    RFID.Delay(150);
    soft_version = RFID.Query_software_version();
  }

  Serial.println("Please approach the RFID card you need to use");
}

void loop() {
  M5.update();
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
  }

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   Query the card information once 查询一次卡的信息例子
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  card = RFID.A_single_poll_of_instructions();
  if (card._ERROR.length() != 0) {
    // Serial.println(card._ERROR);
  } else {
    if (card._EPC.length() == 24) {
      Serial.println("RSSI :" + card._RSSI);
      Serial.println("PC :" + card._PC);
      Serial.println("EPC :" + card._EPC);
      Serial.println("CRC :" + card._CRC);
      Serial.println(" ");

      M5.lcd.fillScreen(BLACK);
      M5.lcd.setCursor(0, 0);
      //M5.Lcd.printf("ms: %d", currentMillis);
      M5.Lcd.println("RSSI :" + card._RSSI);
      M5.Lcd.println("PC :" + card._PC);
      M5.Lcd.println("EPC :" + card._EPC);
    }
  }
  RFID.clean_data(); // Empty the data after using it
                     // 使用完数据后要将数据清空

  /*Other feature usage examples 其他功能使用例子*/

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Read multiple RFID cards at once 一次读取多张RFID卡
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*  cards = RFID.Multiple_polling_instructions(6);
   for (size_t i = 0; i < cards.len; i++)
   {
     if(cards.card[i]._EPC.length() == 24)
       {
          Serial.println("RSSI :" + cards.card[i]._RSSI);
          Serial.println("PC :" + cards.card[i]._PC);
          Serial.println("EPC :" + cards.card[i]._EPC);
          Serial.println("CRC :" + cards.card[i]._CRC);
        }
   }
   Serial.println(" ");
   RFID.clean_data();
   */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Used to get the SELECT parameter 用于获取Select参数
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  //  Select = RFID.Get_the_select_parameter();
  //  if(Select.Mask.length() != 0)
  //  {
  //    Serial.println("Mask :" + Select.Mask);
  //    Serial.println("SelParam :" + Select.SelParam);
  //    Serial.println("Ptr :" + Select.Ptr);
  //    Serial.println("MaskLen :" + Select.MaskLen);
  //    Serial.println("Truncate :" + Select.Truncate);
  //    Serial.println(" ");
  //  }
  //    RFID.clean_data();

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Used to change the PSF bit of the NXP G2X label 用于改变 NXP G2X 标签的
    PSF 位
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  //  Cardinformation = RFID.NXP_Change_EAS(0x00000000);
  //  if(Cardinformation._UL.length() != 0)
  //  {
  //    Serial.println("UL :" + Cardinformation._UL);
  //    Serial.println("PC :" + Cardinformation._PC);
  //    Serial.println("EPC :" + Cardinformation._EPC);
  //    Serial.println("Parameter :" + Cardinformation._Parameter);
  //    Serial.println("ErrorCode :" + Cardinformation._ErrorCode);
  //    Serial.println("Error :" + Cardinformation._Error);
  //    Serial.println("Data :" + Cardinformation._Data);
  //    Serial.println("Successful :" + Cardinformation._Successful);
  //    Serial.println(" ");
  //   }
  //    RFID.clean_data();

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Used to get the Query parameters 用于获取Query参数
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  //  Query = RFID.Get_the_Query_parameter();
  //  if(Query.QueryParameter.length() != 0)
  //  {
  //    Serial.println("QueryParameter :" + Query.QueryParameter);
  //    Serial.println("DR :" + Query.DR);
  //    Serial.println("M :" + Query.M);
  //    Serial.println("TRext :" + Query.TRext);
  //    Serial.println("Sel :" + Query.Sel);
  //    Serial.println("Session :" + Query.Session);
  //    Serial.println("Targetta :" + Query.Target);
  //    Serial.println("Q :" + Query.Q);
  //    Serial.println(" ");
  //  }
  //  RFID.clean_data();

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Used to get the Query parameters 用于读取接收解调器参数
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  //  Read = RFID.Read_receive_demodulator_parameters();
  //  if(Read.Mixer_G.length()!= 0)
  //  {
  //    Serial.println("Mixer_G :" + Read.Mixer_G);
  //    Serial.println("IF_G :" + Read.IF_G);
  //    Serial.println("Thrd :" + Read.Thrd);
  //    Serial.println(" ");
  //  }
  //  RFID.clean_data();
}
