# 1 "/var/folders/44/zftr3xj92fn8yl2byq89ph9m0000gn/T/tmpunbzk3ll"
#include <Arduino.h>
# 1 "/Volumes/Protected/Projects/Arduino/m5_uhf/src/main.ino"

#include <Arduino.h>
#include "wifi.h"
#include <HTTPClient.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <WiFiMulti.h>
#include <M5StickCPlus.h>
#include "RFID_command.h"

UHF_RFID RFID;

String comd = " ";
CardpropertiesInfo card;
ManyInfo cards;
SelectInfo Select;
CardInformationInfo Cardinformation;
QueryInfo Query;
ReadInfo Read;
TestInfo Test;

long previousMillis = 0;
long interval = 60 * 1000;
void setup();
void loop();
#line 25 "/Volumes/Protected/Projects/Arduino/m5_uhf/src/main.ino"
void setup() {
  M5.begin(true, true, true);
  RFID._debug = 1;
  Serial2.begin(115200, SERIAL_8N1, 33, 32);

  M5.Lcd.setRotation(1);
  M5.Lcd.setTextSize(2);


  M5.Lcd.println("Connecting to WiFi");
  Serial.println("Connecting to WiFi");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.print(".");
    Serial.print(".");
    delay(100);
  }
  M5.Lcd.println(" CONNECTED");
  Serial.println(" CONNECTED");

  String soft_version;
  soft_version = RFID.Query_software_version();
  while (soft_version.indexOf("V2.3.5") == -1) {
    Serial.print("Version not V2.3.5, got [");
    Serial.print(soft_version);
    Serial.println("]");
    RFID.clean_data();
    RFID.Delay(150);
    RFID.Delay(150);
    soft_version = RFID.Query_software_version();
  }

  Serial.println("Please approach the RFID card you need to use");
}

void loop() {
  M5.update();
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
  }




  card = RFID.A_single_poll_of_instructions();
  if (card._ERROR.length() != 0) {

  } else {
    if (card._EPC.length() == 24) {
      Serial.println("RSSI :" + card._RSSI);
      Serial.println("PC :" + card._PC);
      Serial.println("EPC :" + card._EPC);
      Serial.println("CRC :" + card._CRC);
      Serial.println(" ");

      M5.lcd.fillScreen(BLACK);
      M5.lcd.setCursor(0, 0);

      M5.Lcd.println("RSSI :" + card._RSSI);
      M5.Lcd.println("PC :" + card._PC);
      M5.Lcd.println("EPC :" + card._EPC);
    }
  }
  RFID.clean_data();
# 176 "/Volumes/Protected/Projects/Arduino/m5_uhf/src/main.ino"
}